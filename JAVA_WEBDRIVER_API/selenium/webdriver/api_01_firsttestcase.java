package webdriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_01_firsttestcase 
{
	WebDriver driver;
	
	public int RandomInt()
	{
		Random rand = new Random();

		int  number = rand.nextInt(900000) + 1;
		 return number;
	}
	
	@BeforeClass
	public void beforeClass() 
	{
		driver = new FirefoxDriver();
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	@Test
	public void TC_01()
	{
		//Step 2; Verify the title is a Home Page
		String homepageTitle = driver.getTitle();
		Assert.assertEquals("Home page", homepageTitle);
		
		//Step 3: Go to the Login Page
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();
		
		//Step 4. Go to Register page
		driver.findElement(By.xpath("//a[contains(@title,'Create an Account')]")).click();
		
		//Step 5: Back to Login page
		driver.navigate().back();
		String loginURL = driver.getCurrentUrl();
		//Verify the current URL is: http://live.guru99.com/index.php/customer/account/login/
		Assert.assertEquals(loginURL, "http://live.guru99.com/index.php/customer/account/login/");
		
		//Step 6: Click on forward button
		driver.navigate().forward();
		String registerURL = driver.getCurrentUrl();
		//Verify the current URL is: http://live.guru99.com/index.php/customer/account/create/
		Assert.assertEquals(registerURL, "http://live.guru99.com/index.php/customer/account/create/");
	}
	
	@Test
	public void TC_02LoginWithEmptyInformation() 
	{
		// Step 02: Click on My Account link
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();
		
		
		// Step 03 - Empty Username/ Password 
		driver.findElement(By.xpath(".//*[@id='email']")).sendKeys("");
		driver.findElement(By.xpath(".//*[@id='pass']")).sendKeys("");
		// Click Login button
		driver.findElement(By.xpath("//*[@id='send2']")).click();

		// Step 05: Get the error message and verify them 
		String emailError = driver.findElement(By.xpath("//*[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals("This is a required field.", emailError);
				
		String passwordError = driver.findElement(By.xpath("//*[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals("This is a required field.", passwordError);		
	}
	
	@Test
	public void TC_03LoginWithEmailIncorrect()
	{
		
		// Step 02: Click on My Account link
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();
		
		//Step 03 - Nhập email invalid: 123434234@12312.123123
		driver.findElement(By.xpath(".//*[@id='email']")).sendKeys("123434234@12312");
		driver.findElement(By.xpath(".//*[@id='pass']")).sendKeys("123123");
		
		//Step 4: Click on Login button
		driver.findElement(By.xpath(".//*[@id='send2']")).click();
		
		String invalidEmail = driver.findElement(By.xpath(".//*[@id='advice-validate-email-email']")).getText();
		Assert.assertEquals("Please enter a valid email address. For example johndoe@domain.com.", invalidEmail);
	}
	
	@Test
	public void TC_04LoginWithPasswordIncorrect()
	{
		// Step 02: Click on My Account link
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();
		
		//Step 03 - Nhập email valid and password incorrect: automation@gmail.com/ 123
		driver.findElement(By.xpath(".//*[@id='email']")).sendKeys("automation@gmail.com");
		driver.findElement(By.xpath(".//*[@id='pass']")).sendKeys("123");
				
		//Step 4: Click on Login button
		driver.findElement(By.xpath(".//*[@id='send2']")).click();
				
		String invalidPassword = driver.findElement(By.xpath(".//*[@id='advice-validate-password-pass']")).getText();
		Assert.assertEquals("Please enter 6 or more characters without leading or trailing spaces.", invalidPassword);		
	}
	
	@Test
	public void TC_05()
	{		
		
		/// Step 02: Click on My Account link
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();
		
		//Step 3 - Click on Create an Account button
		driver.findElement(By.xpath("//a[contains(@title,'Create an Account')]")).click();
		
		//Step 04 - Nhập thông tin hợp lệ vào tất cả các field: First Name/ Last Name/ Email Address/ Password/ Confirm Password
		driver.findElement(By.xpath(".//*[@id='firstname']")).sendKeys("Tuan");
		driver.findElement(By.xpath(".//*[@id='middlename']")).sendKeys("Thanh");
		driver.findElement(By.xpath(".//*[@id='lastname']")).sendKeys("Nguyen");
		
		//Create a random email address
		
		driver.findElement(By.xpath(".//*[@id='email_address']")).sendKeys("harry.auto" + RandomInt() + "@hotmail.com");
		driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("123456789@a");
		driver.findElement(By.xpath(".//*[@id='confirmation']")).sendKeys("123456789@a");
		
		//Step 05 - Verify message xuất hiện khi đăng kí thành công: 
		driver.findElement(By.xpath("//button[@title='Register']")).click();
		String successMsg = driver.findElement(By.xpath("//li[@class='success-msg']//span")).getText();
		Assert.assertEquals(successMsg, "Thank you for registering with Main Website Store.");
		
		
		//Step 6: Logout
		//Click on Account link
		driver.findElement(By.xpath("//div[@class='account-cart-wrapper']//a[contains(@class,'skip-account')]")).click();
		
		WebElement elementLogOut = driver.findElement(By.xpath("//a[@title='Log Out']"));
		if(elementLogOut.isDisplayed())
		{
			elementLogOut.click();
		}
		else
		{
			System.out.println("Log Out element is not displayed");
		}
		
		String homepageTitleLogOut = driver.getTitle();
		Assert.assertEquals("Magento Commerce", homepageTitleLogOut);
	}
		
	@AfterClass
	public void afterClass() 
	{
		driver.quit();
	}

}
