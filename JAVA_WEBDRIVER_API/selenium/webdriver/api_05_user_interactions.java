package webdriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_05_user_interactions {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver",
				"D:\\Exercise Place\\Automation_Advance\\java_webdriver_api\\JAVA_WEBDRIVER_API\\webdriver\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void TC_01ToolTipDisplays() {

		// Access into the website
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Mouse hover to 'Hover over me'
		WebElement hover = driver.findElement(By.xpath("//a[contains(text(),'Hover over me')]"));

		Actions action = new Actions(driver);
		action.moveToElement(hover).perform();

		String sTooltip = driver.findElement(By.xpath("//div[@class='tooltip-inner']")).getText();

		Assert.assertEquals("Hooray!", sTooltip);

	}

	@Test
	public void TC_02VerifyLoginForm() {

		// Access into the website
		driver.get("http://www.myntra.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Mouse hover to Menu to login
		WebElement eMenu = driver.findElement(By.xpath(".//div[@class='desktop-userIconsContainer']"));

		Actions action = new Actions(driver);
		action.moveToElement(eMenu).perform();

		// Click on Login button
		driver.findElement(By.xpath(".//a[contains(text(),'login')]")).click();

		WebElement loginForm = driver.findElement(By.xpath(".//div[@class='login-box']"));

		// Verify that login form displayed
		Assert.assertTrue(loginForm.isDisplayed());
	}

	@Test
	public void TC_03ClickandHoldElement() throws Exception {

		// Access into the web site
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Get All Web Element
		List<WebElement> listElement = driver.findElements(By.xpath("//*[@id='selectable']/li"));
		Actions action = new Actions(driver);

		// Click and Hold from 1 to 4
		action.clickAndHold(listElement.get(0)).clickAndHold(listElement.get(3)).click().perform();
		action.release();

		// Get all Element was selected
		List<WebElement> listElementSelected = driver
				.findElements(By.xpath("//li[@class='ui-state-default ui-selectee ui-selected']"));
		int number = listElementSelected.size();

		// Verify there are 4 elements had been selected
		Assert.assertEquals(4, number);
		Thread.sleep(40);
	}

	@Test
	public void TC_04Double_Click() throws Exception {
		// Access into the web site
		driver.get("http://www.seleniumlearn.com/double-click");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement btnDouble = driver.findElement(By.xpath(".//button[contains(text(),'Double-Click Me!')]"));
		Actions doubleClick = new Actions(driver);

		// Double click on the button
		doubleClick.doubleClick(btnDouble).perform();

		Thread.sleep(4000);
		Alert alert = driver.switchTo().alert();
		// Verify the text displays in Alert
		Assert.assertEquals("The Button was double-clicked.", alert.getText());

		// Click on OK button on Alert
		alert.accept();

	}

	@Test
	public void TC_05Right_Click() throws Exception {
		// Access into the web site
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Right click on button
		WebElement btnRight = driver.findElement(By.xpath("//span[contains(text(),'right click me')]"));
		Actions aRightClick = new Actions(driver);

		// Perform right click on button
		aRightClick.contextClick(btnRight).build().perform();
		Thread.sleep(5000);

		WebElement menuQuit = driver.findElement(By.xpath("//li[contains(.,'Quit')]"));
		Actions hover = new Actions(driver);
		hover.moveToElement(menuQuit).perform();
		Thread.sleep(4000);

		WebElement quitButtonHover = driver
				.findElement(By.xpath("//li[contains(@class,'context-menu-visible') and contains(.,'Quit')]"));
		Assert.assertTrue(quitButtonHover.isDisplayed());

		menuQuit.click();
		Thread.sleep(4000);

		Alert rightAlert = driver.switchTo().alert();
		rightAlert.accept();
		Thread.sleep(4000);
		Assert.assertFalse(quitButtonHover.isDisplayed());

	}

	@Test
	public void TC_05_01Drag_and_Drop() throws Exception {
		// Access into the web site
		driver.get("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// The Source Element
		WebElement eSource = driver.findElement(By.xpath("//*[@id='draggable']"));
		WebElement eDestination = driver.findElement(By.xpath("//*[@id='droptarget']"));

		Actions action = new Actions(driver);
		action.dragAndDrop(eSource, eDestination).perform();
		Thread.sleep(4000);
		Assert.assertEquals("You did great!", eDestination.getText());

	}

	@Test
	public void TC_05_02Drag_and_Drop() throws Exception {
		// Access into the web site
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// The Source Element
		WebElement eSource = driver.findElement(By.xpath("//*[@id='draggable']"));
		WebElement eDestination = driver.findElement(By.xpath("//*[@id='droppable']"));

		Actions action = new Actions(driver);
		action.dragAndDrop(eSource, eDestination).perform();
		Thread.sleep(4000);
		Assert.assertEquals("Dropped!", eDestination.getText());

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
