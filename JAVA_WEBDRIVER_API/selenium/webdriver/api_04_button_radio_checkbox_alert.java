package webdriver;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class api_04_button_radio_checkbox_alert {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
	}

	@Test
	public void TC01_Button() throws Exception {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.findElement(By.xpath(".//*[@id='button-enabled']")).click();

		Assert.assertEquals("http://daominhdam.890m.com/#", driver.getCurrentUrl());

		// Back to previous page
		driver.navigate().back();
		Thread.sleep(2000);
		// Click on button by JavaScript
		WebElement btnButtonEnabled = driver.findElement(By.xpath(".//*[@id='button-enabled']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnButtonEnabled);
		Assert.assertEquals("http://daominhdam.890m.com/#", driver.getCurrentUrl());
	}

	@Test
	public void TC02_Checkbox() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement chkDualZone = driver.findElement(By.xpath(
				"//*[@id='example']//li/label[contains(text(),'Dual-zone air conditioning')]/preceding-sibling::input"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", chkDualZone);
		// chkDualZone.click();
		Assert.assertTrue(chkDualZone.isSelected());

		if (chkDualZone.isSelected()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", chkDualZone);
			// chkDualZone.click();
			Assert.assertFalse(chkDualZone.isSelected());
		}
	}

	@Test
	public void TC03_Radio() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Click on Radio button: 2.0 Petrol, 147kW
		WebElement rdPetrol = driver.findElement(By
				.xpath("//*[@id='example']//li/label[contains(text(),'2.0 Petrol, 147kW')]/preceding-sibling::input"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", rdPetrol);

		// Verify that if the radio button is not checked and re-check
		if (!rdPetrol.isSelected()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", rdPetrol);
		}
	}

	@Test
	public void TC04_Alerts() throws Exception {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Click on button: Click for JS Alert
		driver.findElement(By.xpath("//button[contains(text(),'Click for JS Alert')]")).click();
		Thread.sleep(2000);
		
		// Verify message in alert is I am a JS Alert
		Alert alertFirst = driver.switchTo().alert();
		Assert.assertEquals("I am a JS Alert", alertFirst.getText());

		// Accept the alert and verify the text
		alertFirst.accept();
		Assert.assertEquals("You clicked an alert successfully",
				driver.findElement(By.xpath(".//*[@id='result']")).getText());
	}

	@Test
	public void TC05_Alerts() throws Exception {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Click on button: Click for JS Alert
		driver.findElement(By.xpath("//button[contains(text(),'Click for JS Confirm')]")).click();
		Thread.sleep(2000);

		// Verify message in alert is I am a JS Alert
		Alert alertSecond = driver.switchTo().alert();
		Assert.assertEquals("I am a JS Confirm", alertSecond.getText());

		// Click on Cancel button
		alertSecond.dismiss();
		Assert.assertEquals("You clicked: Cancel", driver.findElement(By.xpath(".//*[@id='result']")).getText());

	}

	@Test
	public void TC06_Alerts() throws Exception {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Click on button: Click for JS Alert
		driver.findElement(By.xpath("//button[contains(text(),'Click for JS Prompt')]")).click();
		Thread.sleep(2000);
		// Verify message in alert is I am a JS Alert
		Alert alertThird = driver.switchTo().alert();
		Assert.assertEquals("I am a JS prompt", alertThird.getText());

		// Enter the text into the textbox
		String sTextBox = "TuanNguyenAuto";
		alertThird.sendKeys(sTextBox);

		// Click on OK button and verify the text
		alertThird.accept();
		Assert.assertEquals("You entered: "+sTextBox, driver.findElement(By.xpath(".//*[@id='result']")).getText());

	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
