package webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_07_Javascript_Executor {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();

		/**
		 * String exePath = "C:\\BrowserDriver\\IEDriverServer.exe";
		 * System.setProperty("webdriver.ie.driver",
		 * "C:\\BrowserDriver\\IEDriverServer.exe"); driver = new
		 * InternetExplorerDriver();
		 * 
		 * 
		 * 
		 * driver.get("http://live.guru99.com/");
		 * driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 * driver.manage().window().maximize();
		 */
	}

	public void TC_01() throws Exception {
		// Step 02: Use JE to get the page domain
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String domainPage = (String) js.executeScript("return document.domain;");
		Assert.assertEquals("live.guru99.com", domainPage);

		// Step 03: Get page URL
		String urlPage = (String) js.executeScript("return document.URL;");
		Assert.assertEquals("http://live.guru99.com/", urlPage);

		// Use JE to open Mobile page
		WebElement mobileLink = driver.findElement(By.xpath("//a[contains(text(),'Mobile')]"));
		js.executeScript("arguments[0].style.border='6px groove red'", mobileLink);
		Thread.sleep(4000);
		js.executeScript("arguments[0].click();", mobileLink);
		Thread.sleep(2000);
		// Step 05: Add Samsung into Cart
		WebElement samsungProduct = driver.findElement(By
				.xpath("//h2[a[contains(text(),'Samsung Galaxy')]]/following-sibling::div[@class='actions']//button"));
		js.executeScript("arguments[0].style.border='6px groove red'", samsungProduct);
		Thread.sleep(4000);
		js.executeScript("arguments[0].click();", samsungProduct);
		Thread.sleep(2000);
		// Step 06: Verify the message
		String messageDisplays = (String) js.executeScript("return document.documentElement.innerText;");
		Assert.assertTrue(messageDisplays.contains("Samsung Galaxy was added to your shopping cart."));

		// Step 7: Open Privacy Policy by JE
		WebElement privacyPolicy = driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]"));
		js.executeScript("arguments[0].style.border='6px groove red'", privacyPolicy);
		Thread.sleep(4000);
		js.executeScript("arguments[0].click();", privacyPolicy);
		Thread.sleep(2000);
		String privacyPageTitle = (String) js.executeScript("return document.title;");
		Assert.assertEquals("Privacy Policy", privacyPageTitle);

		// Scroll to bottom of page
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");

		// Verify the data displays
		WebElement eWishlistDisplay = driver
				.findElement(By.xpath("//th[contains(text(),'WISHLIST_CNT')]/following-sibling::td"));
		js.executeScript("arguments[0].style.border='6px groove red'", eWishlistDisplay);
		Thread.sleep(4000);
		Assert.assertTrue(eWishlistDisplay.isDisplayed());

		// Step Navigate to other page
		js.executeScript("window.location = 'http://demo.guru99.com/v4/'");
		String newDomain = (String) js.executeScript("return document.domain;");
		Assert.assertEquals("demo.guru99.com", newDomain);

	}

	@Test
	public void TC_01_RemoveAttribute() throws Exception {
		driver = new FirefoxDriver();
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement iframeResult = driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(iframeResult);
		// Last Name Element
		WebElement lastName = driver.findElement(By.xpath("//input[@name='lname']"));
		
		removeAttributeInDOM(driver, lastName, "disabled");

		lastName.sendKeys("Automation Testing");
		driver.findElement(By.xpath("//*[@value='Submit']")).click();

		String sResult = driver.findElement(By.xpath("//div[@class='w3-container w3-large w3-border']")).getText();
		Assert.assertEquals("fname=&lname=Automation Testing ", sResult);

		Thread.sleep(4000);

	}

	public static void highlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='6px groove red'", element);
	}

	public Object executeForWebElement(WebDriver driver, WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object removeAttributeInDOM(WebDriver driver, WebElement element, String attribute) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object executeForBrowserElement(WebDriver driver, String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
