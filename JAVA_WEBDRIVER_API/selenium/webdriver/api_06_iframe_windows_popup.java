package webdriver;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_06_iframe_windows_popup {
	WebDriver driver;

	public int RandomInt() {
		Random rand = new Random();

		int number = rand.nextInt(900000) + 1;
		return number;
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();

	}

	// @Test
	public void TC_01VerifyText_Image() {
		driver.get("https://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Create web element for the firt iframe
		WebElement iframeFlipper = driver.findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		// Switch to this iframe
		driver.switchTo().frame(iframeFlipper);

		// Declare the web element which contains the text
		WebElement eText = driver.findElement(By.xpath("//*[@id='messageText']"));
		Assert.assertEquals("What are you looking for?", eText.getText());

		// Return to Homepage Content
		driver.switchTo().defaultContent();

		// Create the Ifram which contain 6 images
		WebElement iframeSiblingBanner = driver.findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		// Switch to this iframe
		driver.switchTo().frame(iframeSiblingBanner);

		// Declare the Banner Web Element
		List<WebElement> lBanner = driver.findElements(By.xpath("//div[@class='bannerimage-container']"));
		int number = lBanner.size();

		Assert.assertEquals(6, number);

		// Return to Homepage Content
		driver.switchTo().defaultContent();

		// Verify flipper banner displayed
		WebElement eFlipperBanner = driver.findElement(By.xpath("//div[@class='flipBanner']"));
		Assert.assertTrue(eFlipperBanner.isDisplayed());
	}

	// @Test
	public void TC_02VerifyNewWindows() throws Exception {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Get the Parent ID for Parent Windows
		String parentID = driver.getWindowHandle();

		// Click on Click Here link
		driver.findElement(By.xpath("//a[contains(text(),'Click Here')]")).click();
		Thread.sleep(4000);
		// Switch to new windows
		switchToWindowByID(parentID);

		Assert.assertEquals("Google", driver.getTitle());

		// Close New Windows and get back to Parent Windows

		Assert.assertTrue(closeAllWindowsWithoutParent(parentID));
		Thread.sleep(4000);

	}

	@Test
	public void TC_03_Windows_Popup() throws Exception 
	{
		driver.get("https://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		String hdfcParentID = driver.getWindowHandle();

		// div[@id='vizury-notification-container']//iframe
		// *[@id='div-close']
		// Click on Angri link
		driver.findElement(By.xpath("//a[contains(text(),'Agri')]")).click();
		// Switch to new windows
		switchToWindowByTitle(hdfcParentID);
		Thread.sleep(4000);
		String titleParentID = driver.getWindowHandle();
		// Click on Account Details link
		WebElement accountLink = driver.findElement(By.xpath("//a[contains(.,'Account Details')]"));
		Assert.assertTrue(accountLink.isDisplayed());
		accountLink.click();
		// Switch to new tab
		switchToWindowByTitle(titleParentID);
		Thread.sleep(4000);

		WebElement thridFrame = driver.findElement(By.xpath("//frame[@name='footer']"));
		// Switch to Frame
		driver.switchTo().frame(thridFrame);
		// Click on Privacy Policy link
		driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]")).click();

		// Back to Main Windows and Closed all
		boolean checkClosenBack = closeAllWindowsWithoutParent(hdfcParentID);
		Assert.assertTrue(checkClosenBack);
		Thread.sleep(4000);

	}

	public void switchToWindowByID(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindows : allWindows) {
			if (!childWindows.equals(parent)) {
				driver.switchTo().window(childWindows);
				break;
			}
		}
	}

	public void switchToWindowByTitle(String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindows : allWindows) {
			driver.switchTo().window(childWindows);
			String childTitle = driver.getTitle();
			if (childTitle.equals(title)) {
				break;
			}
		}
	}

	public boolean closeAllWindowsWithoutParent(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindows : allWindows) {
			if (!childWindows.equals(parent)) {
				driver.switchTo().window(childWindows);
				driver.close();
			}
		}
		driver.switchTo().window(parent);

		// Assert window = 1
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
