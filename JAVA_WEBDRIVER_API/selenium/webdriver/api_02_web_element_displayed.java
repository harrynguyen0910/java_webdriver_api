package webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_02_web_element_displayed {
	
	WebDriver driver;
	
	@BeforeClass
	public void beforeClass() 
	{
		driver = new FirefoxDriver();
		
	}		
	
	@Test
	public void TC_01VerifyElementDisplayed() 
	{
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		String strText = "Automation Testing";
		//Verify Age element is displayed
		WebElement ageElement = driver.findElement(By.xpath("//*[@id='under_18']"));
		if(ageElement.isDisplayed())
		{
			ageElement.click();
		}
		else
		{
			System.out.println("Age for Under 18 is not displayed");
		}
		
		//Verify Email element is displayed
		WebElement emailElement = driver.findElement(By.xpath("//*[@id='mail']"));
		if(emailElement.isDisplayed())
		{
			emailElement.sendKeys(strText);
		}
		else
		{
			System.out.println("Email element is not displayed");
		}
		
		//Verify Education element is displayed
		WebElement educationElement = driver.findElement(By.xpath("//*[@id='edu']"));
		if(educationElement.isDisplayed())
		{
			educationElement.sendKeys(strText);
		}
		else
		{
			System.out.println("Education Element is not displayed");
		}
		
	}
	
	@Test
	public void TC_02VerifyElementEnableDisabled() 
	{
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		//Step 02 - Verify these elements are enabled on this page: 
		//Email/ Age (Under 18)/ Education/ Job Role 01/ Interests (Development)/ Slider 01/ Button is enabled
		WebElement rdUuder18 = driver.findElement(By.xpath("//*[@id='under_18']"));
		WebElement txtEducation = driver.findElement(By.xpath("//*[@id='edu']"));
		WebElement drpJobRole = driver.findElement(By.xpath("//*[@id='job1']"));
		WebElement chkbDevelop = driver.findElement(By.xpath("//*[@id='development']"));
		WebElement sldSlider01 = driver.findElement(By.xpath("//*[@id='slider-1']"));
		
		if(rdUuder18.isEnabled())
		{
			System.out.println("Radio Under 18 button is enabled");
		}
		else
		{
			System.out.println("Radio Under 18 button is not enabled");
		}
		
		
		if(txtEducation.isEnabled())
		{
			System.out.println("Education button is enabled");
		}
		else
		{
			System.out.println("Education button is not enabled");
		}
		
		if(drpJobRole.isEnabled())
		{
			System.out.println("Job Role button is enabled");
		}
		else
		{
			System.out.println("Job Role button is not enabled");
		}
		
		if(chkbDevelop.isEnabled())
		{
			System.out.println("Develop button is enabled");
		}
		
		else
		{
			System.out.println("Develop button is not enabled");
		}
		
		if(sldSlider01.isEnabled())
		{
			System.out.println("Slider 01 button is enabled");
		}
		else
		{
			System.out.println("Slider 01 button is not enabled");
		}
		
		//Password / Age (Radiobutton is disabled)/ Biography/ Job Role 02/ 
		//Interests (Checkbox is disabled)/ Slider 02/ Button is disabled
		//Verify the password textbox is disabled
		WebElement txtPassword = driver.findElement(By.xpath("//*[@id='password']"));
		String strPassword = txtPassword.getAttribute("placeholder");
		System.out.println("The Password" + strPassword);
		
		WebElement rdDisabled = driver.findElement(By.xpath("//*[@id='radio-disabled']"));
		String strRadioDisabled = rdDisabled.getAttribute("value");
		System.out.println("The Radio button" + strRadioDisabled);
		
		WebElement txtBiography = driver.findElement(By.xpath("//*[@id='bio']"));
		String strBiography = txtBiography.getAttribute("placeholder");
		System.out.println("The Password" + strBiography);
		
		WebElement txtJobRole2 = driver.findElement(By.xpath("//*[@id='job2']"));
		String strJobRole2 = txtJobRole2.getAttribute("disabled");
		System.out.println("The Job Role 2 is " + strJobRole2);
		
		WebElement chkDisabled = driver.findElement(By.xpath(".//*[@id='check-disbaled']"));
		String strDisabled = chkDisabled.getAttribute("value");
		System.out.println("The Check box is " + strDisabled);
		
		WebElement sldSider2 = driver.findElement(By.xpath(".//*[@id='slider-2']"));
		String strSlider2 = sldSider2.getAttribute("disabled");
		System.out.println("The Slider 02 is " + strSlider2);
		
		
		
	}
	
    @Test
    public void TC_03VerifyElementIsSelected() 
    {
    	driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
    	//Step 2: Click on Radio button Under 18 and Interest (Development)
    	WebElement rdUnder18btn = driver.findElement(By.xpath("//*[@id='under_18']"));
    	rdUnder18btn.click();
    	
    	WebElement chkDevelopment = driver.findElement(By.xpath("//*[@id='development']"));
    	chkDevelopment.click();
    	
    	if(rdUnder18btn.isSelected() && chkDevelopment.isSelected())
    	{
    		System.out.println("Radio Under 18 and Development check box are checked");
    	}
    	else
    	{
    		System.out.println("Radio Under 18 and Development check box are not checked");
    	}
    }
	
	@AfterClass
	public void afterClass() 
	{
		driver.quit();
	}

}
