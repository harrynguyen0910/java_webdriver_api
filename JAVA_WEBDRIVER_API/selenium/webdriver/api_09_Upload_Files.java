package webdriver;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class api_09_Upload_Files {
	WebDriver driver;

	String sPath = "C:\\Buttons.png";
	String sFileName = "Buttons.png";
	String sRandomFoler = "Tuan" + RandomInt();
	String sEmailAddress = "harry"+RandomInt()+"@gmail.com";
	String sFirstName = "Tuan Nguyen";

	public void TC_01_SendKeys() throws Exception {

		driver = new FirefoxDriver();
		driver.get("http://www.helloselenium.com/2015/03/how-to-upload-file-using-sendkeys.html");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement eUploadFile = driver.findElement(By.xpath("//input[@name='uploadFileInput']"));
		eUploadFile.sendKeys(sPath);
		Thread.sleep(5000);
	}

	// @Test
	public void TC_02_AutoIT() throws Exception {
		driver = new FirefoxDriver();
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Click on AddFiles button
		WebElement eAddFile = driver.findElement(By.xpath("//input[@name='files[]']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", eAddFile);

		// Execute autoit script to enter the file to upload
		Runtime.getRuntime().exec(new String[] { "..\\JAVA_WEBDRIVER_API\\upload\\firefox.exe", sPath });
		Thread.sleep(3000);

		// Verify the upload file has been uploaded
		boolean bResult = driver
				.findElement(By.xpath(
						"//table[@role='presentation']//p[@class='name' and contains(text()," + sFileName + ")]"))
				.isDisplayed();
		Assert.assertTrue(bResult);
	}
	
	@Test
	public void TC_03_Robot() throws Exception
	{
		driver = new FirefoxDriver();
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		//Specify the file location with extension
		StringSelection select = new StringSelection(sPath);
		
		//Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);

		//Click
		WebElement eAddFile = driver.findElement(By.xpath("//input[@name='files[]']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", eAddFile);

		Robot robot = new Robot();
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
		Thread.sleep(5000);
		// Verify the upload file has been uploaded
				boolean bResult = driver
						.findElement(By.xpath(
								"//table[@role='presentation']//p[@class='name' and contains(text()," + sFileName + ")]"))
						.isDisplayed();
				Assert.assertTrue(bResult);
	}
	//@Test
	public void TC_04_UploadFile() throws Exception {
		driver = new FirefoxDriver();
		driver.get("https://encodable.com/uploaddemo/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Choose File to Upload
		WebElement uploadName = driver.findElement(By.xpath("//input[@id='uploadname1']"));
		// Using Java Executor to click
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", uploadName);

		// Choose file to upload
		Runtime.getRuntime().exec(new String[] { "..\\JAVA_WEBDRIVER_API\\upload\\firefox.exe", sPath });
		Thread.sleep(5000);

		Select uploadLocation = new Select(
				driver.findElement(By.xpath("//select[@class='upform_field picksubdir_field']")));
		uploadLocation.selectByVisibleText("/uploaddemo/files/");

		// Enter the random number
		driver.findElement(By.xpath("//input[@id='newsubdir1']")).sendKeys(sRandomFoler);

		// Enter Email and First Name
		driver.findElement(By.xpath("//input[@id='formfield-email_address']")).sendKeys(sEmailAddress);
		driver.findElement(By.xpath("//input[@id='formfield-first_name']")).sendKeys(sFirstName);

		// Click on Begin Upload foler
		WebElement uploadButton = driver.findElement(By.xpath("//input[@id='uploadbutton']"));
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", uploadButton);
		
		//Verify all information displayed correctly
		WebElement imageDisplayed = driver.findElement(By.xpath("//a[contains(text(),'"+sFileName+"')]"));
		Assert.assertTrue(imageDisplayed.isDisplayed());
		
		WebElement imailDisplayed = driver.findElement(By.xpath("//div[@id='uploadDoneContainer']//dd[contains(text(),'Email Address: "+sEmailAddress+"')]"));
		Assert.assertTrue(imailDisplayed.isDisplayed());
		
		WebElement fulnameDisplayed = driver.findElement(By.xpath("//div[@id='uploadDoneContainer']//dd[contains(text(),'First Name: "+sFirstName+"')]"));
		Assert.assertTrue(fulnameDisplayed.isDisplayed());
		
		//Click on View Uploaded Files link
		driver.findElement(By.xpath("//a[contains(text(),'View Uploaded Files')]")).click();
		
		//Click on the folder which had been upload file
		driver.findElement(By.xpath("//a[contains(text(),'"+sRandomFoler+"')]")).click();
		//Verify an Image has been uploaded successfully
		
		String imageUploaded = driver.findElement(By.xpath("//a[@id='fclink-imagepng']")).getText();
		Assert.assertEquals(sFileName, imageUploaded);
		
		Thread.sleep(4000);
				
	}

	public int RandomInt() {
		Random rand = new Random();

		int number = rand.nextInt(900000) + 1;
		return number;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
