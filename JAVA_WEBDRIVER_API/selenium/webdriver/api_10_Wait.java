package webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class api_10_Wait {
	WebDriver driver;
	int waitTime = 15;

	// @Test
	public void TC_01_Implicit_Wait() {
		driver = new FirefoxDriver();
		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement buttonStart = driver.findElement(By.xpath(".//*[@id='start']/button"));
		buttonStart.click();

		String helloText = driver.findElement(By.xpath(".//*[@id='finish']/h4")).getText();
		Assert.assertEquals("Hello World!", helloText);

	}

	// @Test
	public void TC_02_Explicit_Wait() {
		driver = new FirefoxDriver();
		driver.get(
				"http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Declare a wait parameter
		WebDriverWait wait = new WebDriverWait(driver, waitTime);

		// Wait for "Date Time Picker" displayed
		wait.until(ExpectedConditions
				.presenceOfAllElementsLocatedBy(By.xpath("//*[@id='ctl00_ContentPlaceholder1_Panel1']/div")));

		WebElement lableDisplay = driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceholder1_Label1']"));
		Assert.assertEquals("No Selected Dates to display.", lableDisplay.getText().trim());

		// Click on Date Time to choose today
		driver.findElement(By.xpath("//td[a[contains(text(),'12')]]")).click();

		// Wait for until "loader ajax" invisible
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='raDiv']")));

		// Wait for the selected date is visible
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//*[contains(@class,'rcSelected')]//a[text()='12']")));
		WebElement lableDisplayAfter = driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceholder1_Label1']"));
		Assert.assertEquals("Tuesday, December 12, 2017", lableDisplayAfter.getText().trim());
	}

	// @Test
	public void TC_03_Fluent_Wait_01() {
		driver = new FirefoxDriver();
		driver.get("https://stuntcoders.com/snippets/javascript-countdown/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Declare a wait parameter
		WebDriverWait wait = new WebDriverWait(driver, waitTime);

		WebElement countDown = driver.findElement(By.xpath("//*[@id='javascript_countdown_time']"));

		wait.until(ExpectedConditions.visibilityOf(countDown));

		new FluentWait<WebElement>(countDown).withTimeout(10, TimeUnit.SECONDS).pollingEvery(01, TimeUnit.SECONDS)
				.until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement element) {
						boolean flag = element.getText().endsWith("02");
						return flag;
					}
				});

	}

	@Test
	public void TC_03_Fluent_Wait_02() {

		driver = new FirefoxDriver();
		driver.get("http://toolsqa.wpengine.com/automation-practice-switch-windows/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Declare a wait parameter
		WebDriverWait wait = new WebDriverWait(driver, waitTime);

		// wait for count down timer visible
		WebElement countDown_2 = driver.findElement(By.xpath("//*[@id='clock']"));
		wait.until(ExpectedConditions.visibilityOf(countDown_2));

		// Check change color after 5s
		new FluentWait<WebElement>(countDown_2).withTimeout(50, TimeUnit.SECONDS).pollingEvery(05, TimeUnit.SECONDS)
				.until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement changeColor) {
						changeColor = driver.findElement(By.xpath("//*[@style='color: red;']"));
						return changeColor.isDisplayed();
					}
				});

		// Verify the Buzz Buzz displays every 10s
		new FluentWait<WebElement>(countDown_2).withTimeout(50, TimeUnit.SECONDS).pollingEvery(05, TimeUnit.SECONDS)
				.until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement textChange) {
						textChange = driver.findElement(By.xpath("//*[@id='clock']"));
						boolean check = textChange.getText().contains("Buzz Buzz");
						return check;

					}
				});

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
