package webdriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_03_Dropdown_TextArea {

	WebDriver driver;
	public String sCustomerName, sDateOfBirth, sAddress, sCity, sState, sPin, sUserID, sMobile, sEmail, sPass,
			sUserName;
	public String sNewAddress, sNewCity;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();

		sCustomerName = "Tuan Nguyen Auto";
		sDateOfBirth = "10/10/2000";
		sAddress = "1235 Quang Trung";
		sCity = "TPHCM";
		sState = "Go Vap";
		sPin = "700000";
		sMobile = "123456789";
		sEmail = "harry.auto" + RandomInt() + "@hotmail.com";
		sPass = "eturAsA";
		sUserName = "mngr105941";
		sNewAddress = "1937 Vanh Dai Phi Truong";
		sNewCity = "Can Tho";
	}

	@Test
	public void TC_01Verify_DropdownList() {
		// Step 01:
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Step 02 - Kiểm tra dropdown Job Role 01 không hỗ trợ thuộc tính
		// multi-select
		Select drdJobRole01 = new Select(driver.findElement(By.xpath("//*[@id='job1']")));
		Assert.assertFalse(drdJobRole01.isMultiple());

		// Step 03 - Chọn giá trị Automation Tester trong dropdown bằng phương
		// thức selectVisible
		drdJobRole01.selectByVisibleText("Automation Tester");

		// Step 04 - Kiểm tra giá trị đã được chọn thành công
		Assert.assertEquals("Automation Tester", drdJobRole01.getFirstSelectedOption().getText());

		// Step 05 - Chọn giá trị Manual Tester trong dropdown bằng phương thức
		// selectValue
		drdJobRole01.selectByValue("manual");

		// Step 06 - Kiểm tra giá trị đã được chọn thành công
		Assert.assertEquals("Manual Tester", drdJobRole01.getFirstSelectedOption().getText());

		// Step 07 - Chọn giá trị Mobile Tester trong dropdown bằng phương thức
		// selectIndex
		drdJobRole01.selectByIndex(3);

		// Step 08 - Kiểm tra giá trị đã được chọn thành công
		Assert.assertEquals("Mobile Tester", drdJobRole01.getFirstSelectedOption().getText());

		// Step 09 - Kiểm tra dropdown có đủ 5 giá trị
		int isize = drdJobRole01.getOptions().size();
		Assert.assertEquals(5, isize);
	}

	@Test
	public void TC_02Process_Textbox_TextArea() throws Exception {
		// Step 01
		driver.get("http://demo.guru99.com/v4");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Step 02: Login with Username and Password: User = mngr105941 | Pass =
		// eturAsA
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(sUserName);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(sPass);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();

		// Click on New Customer link
		driver.findElement(By.xpath("//a[contains(text(),'New Customer')]")).click();
		// Enter all information and click Submit button

		driver.findElement(By.xpath("//input[@name='name']")).sendKeys(sCustomerName);
		driver.findElement(By.xpath("//input[@name='rad1']")).click();
		driver.findElement(By.xpath("//*[@id='dob']")).sendKeys(sDateOfBirth);
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys(sAddress);
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(sCity);
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys(sState);
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys(sPin);
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys(sMobile);
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys(sEmail);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(sPass);

		driver.findElement(By.xpath("//input[@type='submit']")).click();

		// Get Customer ID
		String sCustomerID = driver
				.findElement(By.xpath(".//*[@id='customer']//td[contains(text(),'Customer ID')]/following-sibling::td"))
				.getText();

		// Click on Edit Customer link
		driver.findElement(By.xpath("//a[contains(text(),'Edit Customer')]")).click();

		// Enter Customer ID
		driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys(sCustomerID);
		driver.findElement(By.xpath("//input[@type='submit']")).click();

		// Get value of Customer Name and Address and Verify
		String sName = driver.findElement(By.xpath("//input[@name='name']")).getAttribute("value");
		String sAddress = driver.findElement(By.xpath("//textarea[@name='addr']")).getText();

		Assert.assertEquals(sCustomerName, sName);
		Assert.assertEquals(sAddress, sAddress);

		// Enter new Address and City
		driver.findElement(By.xpath("//textarea[@name='addr']")).clear();
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys(sNewAddress);
		driver.findElement(By.xpath("//input[@name='city']")).clear();
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(sNewCity);

		Thread.sleep(2000);
		// Click on Submit Button
		driver.findElement(By.xpath("//input[@type='submit']")).click();

		// Verify the Address and City modify correct
		String sAddress1 = driver
				.findElement(By.xpath(".//*[@id='customer']//td[contains(text(),'Address')]/following-sibling::td"))
				.getText();
		String sCity = driver
				.findElement(By.xpath(".//*[@id='customer']//td[contains(text(),'City')]/following-sibling::td"))
				.getText();

		Assert.assertEquals(sNewAddress, sAddress1);
		Assert.assertEquals(sNewCity, sCity);

	}

	public int RandomInt() {
		Random rand = new Random();

		int number = rand.nextInt(900000) + 1;
		return number;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
