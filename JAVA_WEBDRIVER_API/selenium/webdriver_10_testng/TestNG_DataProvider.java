package webdriver_10_testng;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class TestNG_DataProvider {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
	}

	@DataProvider(name = "User/Pass")
	public static Object[][] userAndPassword(Method method) {
		Object[][] result = null;
		if (method.getName().equals("TestNG_01")) {
			result = new Object[][] { { "mngr109856", "qArejUn" }, { "mngr109856", "qArejUn" } };

		} else if (method.getName().equals("TestNG_02")) {
			result = new Object[][] { { "", "" }, { "", "" } };
		}

		return result;
	}

	@Test(dataProvider = "User/Pass")
	public void TestNG_01(String userName, String passWord) {
		driver.get("http://demo.guru99.com/v4/");

		// Enter Username and Password and Click Login butt
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(userName);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(passWord);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		
		//Verify that Login successfully
		WebElement messageElement = driver.findElement(By.xpath("//marquee"));
		Assert.assertTrue(messageElement.isDisplayed());
		Assert.assertEquals("Welcome To Manager's Page of Guru99 Bank", messageElement.getText().trim());
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
