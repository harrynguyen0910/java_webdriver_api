package webdriver_10_testng;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterClass;

public class TestNG_Annotation_01 
{
  @Test
  public void TC_01() {
	  System.out.println("TC_01");
  }
  
  @Test
  public void TC_02() {
	  System.out.println("TC_02");
  }
  
  @Test
  public void TC_03() {
	  System.out.println("TC_03");
  }
  
  
  @BeforeClass
  public void beforeClass() {
	  System.out.println("Before Class");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("After Class");
  }
  
  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("Before Suite");
  }
  
  @BeforeGroups
  public void beforeGroup()
  {
	  System.out.println("Before Group");
  }
 
}
