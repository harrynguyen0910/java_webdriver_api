package webdriver_10_testng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestNG_MultiBrowsers 
{
	WebDriver driver;

	
	@BeforeClass
	@Parameters({ "browser" })
	public void initBrowsers(String browser) 
	{
		if(browser.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver",".\\webdriver\\chromedriver.exe");
			driver = new ChromeDriver();
		}else if (browser.equals("firefox"))
		{
			driver = new FirefoxDriver();
		}else if(browser.equals("ie"))
		{
			System.setProperty("webdriver.ie.driver",".\\webdriver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		
	}
	@Parameters({"userName","passWord"})
	@Test
	public void TC_01_Login(String userName, String passWord) 
	{
		driver.get("http://demo.guru99.com/v4/");
		
		//Enter Username and Password and Click Login butt
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(userName);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(passWord);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		
		//Verify that Login successfully
		WebElement messageElement = driver.findElement(By.xpath("//marquee"));
		Assert.assertTrue(messageElement.isDisplayed());
		Assert.assertEquals("Welcome To Manager's Page of Guru99 Bank", messageElement.getText().trim());
		
	}

	@AfterClass
	public void closeBrowsers() 
	{
		driver.quit();
	}

}
